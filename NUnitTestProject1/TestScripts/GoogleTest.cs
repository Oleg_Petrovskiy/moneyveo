﻿using NUnit.Framework;
using NUnitTestProject1.PageObject;

namespace NUnitTestProject1.TestScripts
{
    [TestFixture]
    public class GoogleTest : Base
    {
        [Test,MaxTime(10*1000)]
        [TestCase("selenium webdriver c#","selenium c#", 3)] 
        [TestCase("selenium webdriver c#", "Getting Started", 3)] 
        public void Test1(string searchtext,string returntext , int num)
        {   
            //arrange
            SearchPage searchPage = new SearchPage(driver);
            searchPage.EnterSearchText(searchtext);
            ResaultPage resaultPage = searchPage.ClickSearchButton();
            string expected = returntext;

            //act
            string actual = resaultPage.GetResCaptionByNum(num);

            //assert
            StringAssert.Contains(expected, actual, "failed");
        }
        [Test, MaxTime(10 * 1000)]
        [TestCase("selenium webdriver c#", "selenium c#", 3)]
        [TestCase("selenium webdriver c#", "Selenium C#", 3)]
        public void Test2(string searchtext, string returntext, int num)
        {
            //arrange
            SearchPage searchPage = new SearchPage(driver);
            searchPage.EnterSearchText(searchtext);
            ResaultPage resaultPage = searchPage.ClickSearchButton();
            string expected = returntext.ToLower();

            //act
            string actual = resaultPage.GetResCaptionByNum(num).ToLower();

            //assert
            StringAssert.Contains(expected, actual, "failed");
        }

    }
}
