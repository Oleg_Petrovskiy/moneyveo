﻿using OpenQA.Selenium;
using System.Collections.Generic;
using System.Linq;

namespace NUnitTestProject1.PageObject
{
    public class ResaultPage
    {
        IWebDriver _driver; 
        public ResaultPage(IWebDriver driver)
        {
            _driver = driver;
        }

        List <IWebElement> ResElements => _driver.FindElements(By.XPath("//div/div[@class='g']//h3[@class]")).ToList();
  //     public IWebElement myTestElement => _driver.FindElement(By.XPath("//div/div[@class='g']//h3[@class]"));

        public string GetResCaptionByNum(int num)
        {

            return ResElements[num-1].Text;

        }
    }
}
