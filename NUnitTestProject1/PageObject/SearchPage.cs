﻿using NUnitTestProject1.PageObject;
using OpenQA.Selenium;

namespace NUnitTestProject1
{
    public  class SearchPage
    {
        IWebDriver _driver;

        public SearchPage(IWebDriver driver)
        {
            this._driver = driver;
        }
         IWebElement txtSearch => _driver.FindElement(By.XPath("//input[@class='gLFyf gsfi'][@name='q']"));

         IWebElement btnSearch => _driver.FindElement(By.XPath("//div[@class='VlcLAe']//input[@name='btnK']"));

        

        public void EnterSearchText(string searchtext)
        {
            txtSearch.SendKeys(searchtext);
        }
        public ResaultPage ClickSearchButton()
        {
            btnSearch.Click();
            return new ResaultPage(_driver);
        }
    }
}
