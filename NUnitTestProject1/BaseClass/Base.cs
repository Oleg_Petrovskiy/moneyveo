﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.IO;
using System.Reflection;

namespace NUnitTestProject1
{
    public class Base
    {
        public IWebDriver driver;

        [SetUp]
        public void Initialize()
        {
            driver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(10 * 1000);
           // driver.Url = "http://google.com";
            driver.Navigate().GoToUrl("http://google.com");
        }
        [TearDown]
        public void Close()
        {
            driver.Quit();

        }

    }
}
